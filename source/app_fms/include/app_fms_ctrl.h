#include "../../protobuf/protofiles/msg_ipc.pb-c.h"

/**
 * This function sets the aircraft's flightmode.
 */
void app_fms_set_flight_mode();

/**
 * 
 */
void app_fms_get_flight_mode();

/**
 * 
 */
void app_fms_compute_control_signal();

/**
 * 
 */
void app_fms_get_gps_settings();

/**
 * 
 */
void app_fms_configure_gps();

/**
 * 
 */
void app_fms_get_ins_settings();

/**
 * 
 */
void app_fms_configure_ins();


