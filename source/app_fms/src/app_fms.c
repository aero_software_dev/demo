/**
 * This application, called app_fms, is in charge with:
 * - computing the necessary flight trajectory based on the provided waypoints
 * - control the aircraft flight trajectory
 * - communicating with the flightcontroller which controls the aircraft's attitude
 *
 * @date 2018
 *
 * @page License
 *
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL * BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*---------------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------------*/

#include <stdio.h>

#include <libsocket.h>
#include <libevent.h>

/*---------------------------------------------------------------------------------------------------------------------
 * INTERNAL FUNCTIONS
 *--------------------------------------------------------------------------------------------------------------------*/

static libevent_handler_status_t app_fms_data_received(void *p_socket)
{

    fprintf(stdout, "Child: Received some data. \n");
    uint8_t buffer[50]={0};
    MsgIpc *msg;

    /* Read received data. */
    read(((socket_t *)p_socket)->socketpair_fd[1], buffer, sizeof buffer);

    msg = msg_ipc__unpack(NULL, 4, buffer);	
    if (msg == NULL)
    {
        fprintf(stderr, "error unpacking incoming message\n");
        exit(1);
    }

    fprintf(stdout, "Received data: %d\n", msg->payload);

    write(((socket_t *)p_socket)->socketpair_fd[1], "Hi", 2);

    return LIBEVENT_HANDLER_STATUS_STOP;
}

int main(int p_argc, char *p_argv[])
{
    libevent_t l_loop = {0};

    socket_t l_socket = {0};
    socket_cfg_t l_socket_cfg = 
    {
        .loop = &l_loop,
        .socket_side = SOCKET_CLIENT,
        .socket_domain = SOCKET_PAIR,
        .on_data_received = app_fms_data_received,
        .data = &l_socket
    };

    libevent_create_eventloop(&l_loop);
    libsocket_set_socketfd(&l_socket, atoi(p_argv[0]),SOCKET_PAIR);
    libsocket_init(&l_socket, &l_socket_cfg);

    libevent_start_eventloop(&l_loop);

    return 0;
}
