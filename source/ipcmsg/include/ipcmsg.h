/**
 * This header file defines the structure of the ipc messages.
 *
 * @date 2018
 *
 * @page License
 *
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL X BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef IPCMSG_H
#define IPCMSG_H

/*---------------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------------*/
typedef enum
{
    IPCMSG_TYPE_CMD,

    IPCMSG_TYPE_CONFIG
} IPCMSG_TYPE;

typedef struct 
{
    /** Payload. */
    int payload;
} ipc_msg_cfg_t;

typedef struct
{
    /** Payload. */
    int payload;
} ipc_msg_cmd_t;

typedef union
{
    /** Command message. */
    ipc_msg_cmd_t cmd_msg;

    /** Configuration message. */
    ipc_msg_cfg_t cfg_msg;
} ipc_msg_t;

typedef struct 
{
    IPCMSG_TYPE msg_type;
    
    /** Destination. */
    int header; 

    ipc_msg_t msg;
} ipc_msg_struct_t;

#endif
