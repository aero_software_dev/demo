/**
 * Integration test for UNIX sockets using libsocket.
 *
 * @date 2018
 *
 * @page License
 *
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL X BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*---------------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include <sys/socket.h> 
#include <netinet/in.h> 
#include <sys/types.h>
#include <sys/wait.h>

#include <libsocket.h>
#include <libevent.h>

/*---------------------------------------------------------------------------------------------------------------------
 * DEFINES
 *--------------------------------------------------------------------------------------------------------------------*/

#define BUFFER_SIZE 3

/*---------------------------------------------------------------------------------------------------------------------
 * INTERNAL FUNCTIONS
 *--------------------------------------------------------------------------------------------------------------------*/

/* Callback function invoked upon reception of data. */
libevent_handler_status_t my_callback(socket_t *p_socket)
{
    char buff[BUFFER_SIZE] = {0};

    const ssize_t l_status = read(p_socket->socket_fd, buff, BUFFER_SIZE);
    if (l_status == -1)
    {
        fprintf(stderr, "An error occured while reading: %m.\n");
        return LIBEVENT_HANDLER_STATUS_ERROR;
    }

    /* Checking whether what we sent is what we receive. */
    if (strncmp(buff, "abc", BUFFER_SIZE) == 0)
    {
        fprintf(stdout, "Sent string matches received string.\n");
        return LIBEVENT_HANDLER_STATUS_STOP;
    }
    else
    {
        fprintf(stdout, "Both strings are not equal.\n");
        return LIBEVENT_HANDLER_STATUS_ERROR;
    }
}

/*---------------------------------------------------------------------------------------------------------------------
 * MAIN
 *--------------------------------------------------------------------------------------------------------------------*/

int main (int p_argc, char *p_argv[])
{
    libevent_t l_loop ={0};

    int32_t l_pid = fork();

    switch(l_pid)
    {
        /* An error occured.*/
        case -1:
            fprintf(stderr, "An error occured.\n");
            break;

        /* Server process. */
        case 0:
            {
                socket_t l_server = 
                {
                    .socket_fd = -1,
                };

                socket_cfg_t l_socket_cfg = 
                {
                    .loop = &l_loop,
                    .socket_domain = SOCKET_AF_UNIX,
                    .socket_side = SOCKET_SERVER,
                    .on_data_received = my_callback
                };

                fprintf(stdout, "server\n");
                libevent_create_eventloop(&l_loop);
                libsocket_init(&l_server, &l_socket_cfg);
                libsocket_start(&l_server);
                libsocket_stop(&l_server);
                 
                break;
            }

        /* Client process. */
        default:
            {
                socket_t l_client = 
                {
                    .socket_fd = -1,
                };

                socket_cfg_t l_client_cfg = 
                {
                    .loop = &l_loop,
                    .socket_side = SOCKET_CLIENT,
                    .socket_domain = SOCKET_AF_UNIX
                };

                const char l_data[BUFFER_SIZE] = {'a', 'b', 'c'};

                sleep(3);
                fprintf(stdout, "Client\n");
                libevent_create_eventloop(&l_loop);

                libsocket_init(&l_client, &l_client_cfg);
                (void) libsocket_send_data(&l_client, l_data, (int32_t) sizeof l_client);

                /* Wait for all child processes to terminate. */
                int l_child_rc = 0;
                const int8_t l_status = wait(&l_child_rc);
                if (l_status != 0)
                {
                    fprintf(stderr, "Error while waiting for child process.\n");
                }
        
                libsocket_stop(&l_client);

                if (WEXITSTATUS(l_child_rc) != 0)
                {   printf("Returning -1\n");
                    return -1;
                }
            }
    }

    return 0;
}
