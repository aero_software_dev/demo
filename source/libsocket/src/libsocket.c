#include <libsocket.h>

#include "../../ipcmsg/include/ipcmsg.h"

/*---------------------------------------------------------------------------------------------------------------------
 * GLOBAL VARIABLES
 *-------------------------------------------------------------------------------------------------------------------*/

/* Path to UNIX socket endpoint files. */
static const char g_libsocket_unix_server_socket_path[20] = "/tmp/my_unix_socket";

/* Portnumber for INET sockets. */
static const int PORT = 8080;

/*---------------------------------------------------------------------------------------------------------------------
 * PRIVATE FUNCTIONS
 *-------------------------------------------------------------------------------------------------------------------*/

static libevent_handler_status_t event_handler(void *p_data)
{
    if (p_data == NULL)
    {
        fprintf(stderr, "Invalid user-defined data.\n");
        return LIBEVENT_HANDLER_STATUS_ERROR;
    }

    return ((socket_t *)p_data)->cfg.on_data_received(((socket_t *)p_data)->cfg.data);
}

static void libsocket_serialize_data(ipc_msg_struct_t *p_data, void **p_buffer)
{
    if (p_data->msg_type == IPCMSG_TYPE_CMD)
    {
        MsgIpc l_msg = MSG_IPC__INIT;
        l_msg.payload = 0x02;
        l_msg.header = 123;

        const unsigned l_len = msg_ipc__get_packed_size(&l_msg);

        *p_buffer = malloc(l_len);
        msg_ipc__pack(&l_msg, *p_buffer);
    }
    else
    {
        MsgIpc l_msg = MSG_IPC__INIT;
        l_msg.payload = 0x02;
        l_msg.header = 123;

        (void) l_msg;
    } 
}

/*---------------------------------------------------------------------------------------------------------------------
 * PUBLIC FUNCTIONS
 *-------------------------------------------------------------------------------------------------------------------*/

void libsocket_init(socket_t *p_socket, const socket_cfg_t *p_cfg)
{
    assert(p_socket != NULL);
    assert(p_cfg != NULL);

    switch (p_cfg->socket_domain)
    {
        case SOCKET_AF_INET4:
            if ( (p_socket->socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) 
            {
                fprintf(stderr, "INET socket creation failed: %s.\n", strerror(errno));
                exit(EXIT_FAILURE);
            }

            p_socket->address.in_socket.sin_family = AF_INET;
            p_socket->address.in_socket.sin_addr.s_addr = INADDR_ANY;
            p_socket->address.in_socket.sin_port = htons(PORT);

            break;

        case SOCKET_AF_UNIX:
            if ( (p_socket->socket_fd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0 ) 
            {
                fprintf(stderr, "UNIX socket creation failed: %s.\n", strerror(errno));
                exit(EXIT_FAILURE);
            }

            p_socket->address.un_socket.sun_family = AF_UNIX;
            strncpy(p_socket->address.un_socket.sun_path, g_libsocket_unix_server_socket_path,
                    sizeof g_libsocket_unix_server_socket_path);

            if ((p_cfg->socket_side == SOCKET_SERVER))
            {
                /* Remove potential residual socket file. */
                if (remove(p_socket->address.un_socket.sun_path) == -1 && errno != ENOENT)
                {
                    fprintf(stderr, "Failed to unlink: %s.\n", strerror(errno));
                    exit(EXIT_FAILURE);
                }
            }
            break;

        case SOCKET_PAIR:
             {
                 if ((p_socket->socketpair_fd[0] == 0) && (p_socket->socketpair_fd[1] == 0))
                 {
                     const int l_status = socketpair(AF_UNIX, SOCK_DGRAM, 0, p_socket->socketpair_fd);
                     if (l_status == -1)
                     {
                         fprintf(stderr, "Error occured while creating socketpair: %s\n", strerror(errno));
                         exit(EXIT_FAILURE);
                     }
                 }
                 break;
             }

        default:
            fprintf(stdout, "default\n");
            break;
    }

    p_socket->cfg = *p_cfg;

    if ((p_socket->cfg.socket_side == SOCKET_SERVER) && (p_socket->cfg.socket_domain != SOCKET_PAIR))
    {
        int reuse = 0;
    
        const int l_status = setsockopt(p_socket->socket_fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(reuse));
        if (l_status == -1)
        {
            fprintf(stderr, "Error setting reusability option: %s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
        if (bind(p_socket->socket_fd, (const struct sockaddr *)&p_socket->address.un_socket, sizeof(struct sockaddr_un)) < 0 )
        {
            fprintf(stderr, "Bind failed: %s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
    }

    /* Add socket to eventloop. */
    libevent_cfg_t l_cfg =
    {
        .data = p_socket,
        .fd_handler = event_handler
    };

    if (p_cfg->socket_domain != SOCKET_PAIR)
    {
        l_cfg.target_fd = p_socket->socket_fd;
    }
    else
    {
        if (p_cfg->socket_side == SOCKET_SERVER)
        {
            l_cfg.target_fd = p_socket->socketpair_fd[0];
        }
        else
        {
            l_cfg.target_fd = p_socket->socketpair_fd[1];
        }
    }

    libevent_add_event(p_socket->cfg.loop, &l_cfg);
}

int32_t libsocket_send_data(socket_t *p_socket, const void *p_data, const uint32_t p_data_size)
{
    assert(p_socket != NULL);
    assert(p_data !=NULL);
    assert(p_data_size != 0);

    ssize_t l_tx_result = 0;

    switch(p_socket->cfg.socket_domain)
    {
        case SOCKET_AF_UNIX:
        {
            l_tx_result = sendto(p_socket->socket_fd, p_data, p_data_size, 0,
                                 (const struct sockaddr *)&p_socket->address.un_socket, sizeof(struct sockaddr_un));
            break;
        }
        case SOCKET_AF_INET4:
        {
            l_tx_result = sendto(p_socket->socket_fd, p_data, p_data_size, MSG_CONFIRM,
                                 (const struct sockaddr *)&p_socket->address.in_socket, sizeof(struct sockaddr));
            break;
        }

        case SOCKET_PAIR:
        {
            void *l_buffer ={0};

            libsocket_serialize_data((ipc_msg_struct_t *)p_data, &l_buffer);

            ssize_t l_written_bytes = 0;
            if (p_socket->cfg.socket_side == SOCKET_SERVER)
            {
                l_written_bytes = write(p_socket->socketpair_fd[0], l_buffer, 4);
            }
            else
            {
                l_written_bytes = write(p_socket->socketpair_fd[1], l_buffer, 4);
            }

            if (l_written_bytes == -1)
            {
                fprintf(stderr, "An error occured when writing bytes: %m.\n");
                exit(EXIT_FAILURE);
            }

            break;
        }

        default:
            return -1;
    }

    if (l_tx_result == -1)
    {
        fprintf(stderr, "An error occured during the transmission: %s - %d.\n", strerror(errno), errno);
        exit(EXIT_FAILURE);
    }

    return l_tx_result;
}

void libsocket_start(socket_t *p_socket)
{
    assert(p_socket != NULL);

    libevent_start_eventloop(p_socket->cfg.loop);
}

void libsocket_stop(socket_t *p_socket)
{
    assert(p_socket != NULL);

    libevent_stop_loop(p_socket->cfg.loop);
    close(p_socket->socket_fd);
}

int32_t libsocket_getfd(socket_t *p_socket)
{
    assert(p_socket != NULL);

    return p_socket->socket_fd;
}

void libsocket_set_socketfd(socket_t *p_socket, int p_socket_fd, socket_domain_t p_socket_domain)
{
    assert(p_socket != NULL);

    switch (p_socket_domain)
    {
        case SOCKET_AF_INET4:
            break;

        case SOCKET_AF_UNIX:
            break;

        case SOCKET_PAIR:
                p_socket->socketpair_fd[0] = -1;
                p_socket->socketpair_fd[1] = p_socket_fd;
            break;
    }

}

