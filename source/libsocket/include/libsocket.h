/**
 * @defgroup libsocket Socket library
 * @{
 *
 * @file libsocket.h
 *
 * @date   2018
 *
 * @page License
 *
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL X BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef LIBSOCKET_FILE
#define LIBSOCKET_FILE

/*---------------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <inttypes.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include <libevent.h>
#include "../../protobuf/protofiles/msg_ipc.pb-c.h"

/*---------------------------------------------------------------------------------------------------------------------
 * DATATYPES
 *-------------------------------------------------------------------------------------------------------------------*/

/** Forward declaration of socket_t. */
typedef struct socket_t socket_t;

/**
 * Enum type telling whether a client or server socket is being used.
 * The latter will be binded, while the former will not.
 */
typedef enum
{
    /** Client socket.*/
    SOCKET_CLIENT = 1,

    /** Server socket.*/
    SOCKET_SERVER = 2
} socket_side_t;

/**
 * Socket domain.
 */
typedef enum
{
    /** Unix domain socket. */
    SOCKET_AF_UNIX = 1,

    /** Inet domain socket. */
    SOCKET_AF_INET4 = 2,

    /** Socketpair **/
    SOCKET_PAIR = 3
} socket_domain_t;

/**
 * Structure used to configure a socket_t handle.
 */
typedef struct 
{
    /** Socket's associated eventloop. */
    libevent_t *loop;

    /** Socket side (client or server). */
    socket_side_t socket_side;

    /** Socket domain. */
    socket_domain_t socket_domain;

    /** User-defined callback function, called upon reception of data. */
    libevent_handler_status_t (*on_data_received)(void *p_this);

    /** User-defined data. */
    void *data;
} socket_cfg_t;

/** UNIX or INET address. */
union socket_addr
{
    /** Internet address. */
    struct sockaddr_in in_socket;

    /** Unix address. */
    struct sockaddr_un un_socket;
};

/**
 * Socket_t handle.
 */
struct socket_t
{
    union socket_addr address;

    /** Socket's fd. */
    int32_t socket_fd;

    /** Socketpair fd's */
    int socketpair_fd[2];

    /** User's socket configuration. */
    socket_cfg_t cfg;
};

/*---------------------------------------------------------------------------------------------------------------------
 * PUBLIC FUNCTIONS
 *-------------------------------------------------------------------------------------------------------------------*/

/**
 * This function initializes a socket.
 *
 * @param p_socket A socket_t handle.
 * @param p_cfg    Configuration of a socket_t handle.
 */
void libsocket_init(socket_t *p_socket, const socket_cfg_t *p_cfg);

/**
 * This function is used to transmit data via a socket.
 *
 * @param p_socket    A socket_t handle.
 * @param p_data      Pointer to the data to transmit.
 * @param p_data_size Number of bytes to transmit.
 */
int32_t libsocket_send_data(socket_t *p_socket, const void *p_data, uint32_t p_data_size);

/**
 * This function starts monitoring a socket's fd.
 *
 * @param p_socket A socket_t handle.
 */
void libsocket_start(socket_t *p_socket);

/**
 * This function stops to monitor the socket's fd.
 *
 * @param p_socket A socket_t handle.
 */
void libsocket_stop(socket_t *p_socket);

/**
 * Getter for the socket's fd.
 *
 * @param p_socket A socket_t handle.
 *
 * @return The socket's file descriptor.
 */
int32_t libsocket_getfd(socket_t *p_socket);

/**
 *
 */
void libsocket_set_socketfd(socket_t *p_socket, int p_socket_fd, socket_domain_t p_socket_domain);
#endif

/** @} */

