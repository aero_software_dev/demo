/**
 * @defgroup libevent Event library.
 * @{
 *
 * @file   libevent.h
 * @brief  Eventloop library.
 *
 * @date   2018
 *
 * @page   License
 *
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL X BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef LIBEVENT_FILE
#define LIBEVENT_FILE

/*---------------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <sys/epoll.h>

/*---------------------------------------------------------------------------------------------------------------------
 * DATA TYPES
 *-------------------------------------------------------------------------------------------------------------------*/

/** Possible return statuses for event handlers. */
typedef enum
{
    /** Success, continue loop. */
    LIBEVENT_HANDLER_STATUS_SUCCESS = 0,

    /** Success, stop loop. */
    LIBEVENT_HANDLER_STATUS_STOP = 1,
    
    /** Error occured. */
    LIBEVENT_HANDLER_STATUS_ERROR = 2
} libevent_handler_status_t;

/** Configuration structure. */
typedef struct
{
    /** Fd which has to be monitored. */
    int32_t target_fd;

    /** User-defined data. */
    void *data;

    /** User-defined callback function */
    libevent_handler_status_t (*fd_handler)(void *p_data);
} libevent_cfg_t;

/** Eventloop handle. */
typedef struct
{
    /** Epoll's fd. */
    int32_t epoll_fd;

    /** Handle configuration. */
    libevent_cfg_t cfg;
} libevent_t;

/*---------------------------------------------------------------------------------------------------------------------
 * PUBLIC FUNCTIONS
 *-------------------------------------------------------------------------------------------------------------------*/

/**
 * This function creates an eventloop.
 *
 * @param p_loop A libevent_t eventloop.
 */
void libevent_create_eventloop(libevent_t *p_loop);

/**
 * This function adds an event with a configuration to the given eventloop.
 *
 * @param p_loop A libevent_t eventloop.
 * @param p_cfg  The events configuration
 */
void libevent_add_event(libevent_t *p_loop, libevent_cfg_t *p_cfg);

/**
 * This function starts the given eventloop.
 *
 * @param p_loop A libevent_t eventloop.
 */
void libevent_start_eventloop(libevent_t *p_loop);

/**
 * This function stops the given eventloop.
 *
 * @param p_loop A libevent_t eventloop.
 */
void libevent_stop_loop(libevent_t *p_loop);

#endif

/** @} */
