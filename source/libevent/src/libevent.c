/*---------------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------------*/

#include <libevent.h>

/*---------------------------------------------------------------------------------------------------------------------
 * DEFINES
 *-------------------------------------------------------------------------------------------------------------------*/

#define LIBEVENT_MAX_EVENTS 10

/*---------------------------------------------------------------------------------------------------------------------
 * PUBLIC FUNCTIONS
 *-------------------------------------------------------------------------------------------------------------------*/

void libevent_create_eventloop(libevent_t *p_loop)
{
    assert(p_loop != NULL);

    /* Create epoll instance. */
    p_loop->epoll_fd = epoll_create(O_CLOEXEC);
    if (p_loop->epoll_fd == -1)
    {
        fprintf(stderr, "Failed to create loop: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
}

void libevent_add_event(libevent_t *p_loop, libevent_cfg_t *p_cfg)
{
    assert(p_loop != NULL);

    struct epoll_event l_epoll_event = {0};

    /* Fd ready to read. */
    l_epoll_event.events = EPOLLIN;
    /* Fd to monitor. */
    l_epoll_event.data.fd = p_cfg->target_fd;
    /* Copy context with callback. */
    l_epoll_event.data.ptr = p_loop;

    p_loop->cfg = *p_cfg;

    /* Add fd's to epoll instance. */
    const int8_t l_status = epoll_ctl(p_loop->epoll_fd, EPOLL_CTL_ADD, p_cfg->target_fd, &l_epoll_event);
    if (l_status == -1)
    {
        fprintf(stderr, "Failed to add event: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
}


void libevent_start_eventloop(libevent_t *p_loop)
{
    assert(p_loop != NULL);

    struct epoll_event l_events[LIBEVENT_MAX_EVENTS] = {0};
    int32_t l_status = 0;

    while(1)
    {
        /* Wait for fd to be ready for read operation (blocking call). */
        l_status = epoll_wait(p_loop->epoll_fd, l_events, LIBEVENT_MAX_EVENTS, -1);
        if (l_status == -1)
        {
            fprintf(stderr, "An error occured while waiting: %s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }

        /* Handle all events. */
        for (int i = 0; i < LIBEVENT_MAX_EVENTS; i++)
        {
            /* Check whether fd is ready to be read. */
            if (l_events[i].events & EPOLLIN)
            {
                libevent_t *l_context = l_events[i].data.ptr;

                /* Call callback function corresponding to event. */
                l_status = l_context->cfg.fd_handler(p_loop->cfg.data);
                switch(l_status)
                {
                    case LIBEVENT_HANDLER_STATUS_SUCCESS:
                         break;
                    case LIBEVENT_HANDLER_STATUS_STOP:
                         fprintf(stdout, "Stopping eventloop.\n");
                        return;
                    case LIBEVENT_HANDLER_STATUS_ERROR:
                        fprintf(stderr, "An unexpected erorr occured when handling the eventloop handler.\n");
                        exit(EXIT_FAILURE);
                }
            }
        }
    }
}

void libevent_stop_loop(libevent_t *p_event)
{
    assert(p_event != NULL);

    /* Close epoll's fd. */
    close(p_event->epoll_fd);
}

/**@} */

