#include <app_router_ctrl.h>

static const char g_path_fms_binary[sizeof "app_fms/app_fms"] = "app_fms/app_fms";

void app_router_ctrl_init_system(app_router_t *p_this, app_router_cfg_t *p_cfg)
{
    socket_t l_socket = {0};
    socket_cfg_t l_socket_cfg = 
    {
        .loop = p_cfg->loop,
        .socket_side = SOCKET_SERVER,
        .socket_domain = SOCKET_PAIR,
        .on_data_received = p_cfg->on_data_received,
        .data = p_this
    };

    libevent_create_eventloop(p_cfg->loop);
    libsocket_init(&l_socket, &l_socket_cfg);

    p_this->cfg = *p_cfg;
    p_this->socket = l_socket;

    /*TODO: setup AF_INET socket to communicate with groundstation. */
}

void app_router_ctrl_start_system(app_router_t *p_this)
{
    for (int i=0; i < p_this->cfg.nr_processes; i++)
    {
        const int32_t l_pid = fork();
    
        switch(l_pid)
        {
            /* An error occured.*/
            case -1:
                fprintf(stderr, "An error occured.\n");
                break;
    
            /* Child process. */
            case 0:
                {
                    char l_str[10]= {0};
                    char *l_parameters[10]={0};
    
                    snprintf(l_str, sizeof(int), "%d", p_this->socket.socketpair_fd[1]);

                    l_parameters[0] = l_str;
                    close(p_this->socket.socketpair_fd[0]);
                    const int l_status = execv(g_path_fms_binary, l_parameters);
                    if (l_status == -1)
                    {
                        fprintf(stdout, "Failed to execute child binary.\n");
                        exit(EXIT_FAILURE);
                    }
                    break;
                }
    
            /* Parent process. */
            default:
                break;
        }
    }

    /* Parent app_router. */
    sleep(2);

    ipc_msg_struct_t l_msg = 
    {
        .msg_type = IPCMSG_TYPE_CMD,
        .header = 123,
        .msg.cmd_msg.payload = 321
    };

    const int32_t l_written_bytes = libsocket_send_data(&p_this->socket, &l_msg, 4);
    if (l_written_bytes == -1)
    {
        fprintf(stderr, "Parent: An error occured when writing bytes: %m.\n");
        exit(EXIT_FAILURE);
    }

    libevent_start_eventloop(p_this->cfg.loop);
}

void app_router_ctrl_send_msg_to_process()
{
    /*TODO*/
}

void app_router_ctrl_send_msg_to_ground_station()
{
    /*TODO*/
}
