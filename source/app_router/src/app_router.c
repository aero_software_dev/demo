/**
 * This application, called app_router, is in charge with:
 * - spawning all the necessary child processes 
 * - sending all the requests from the user/groundstation to the concerned processes using IPC
 * - sending messages from the child processes to the groundstation/user
 *
 * @date 2018
 *
 * @page License
 *
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL X BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*---------------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include <sys/socket.h> 
#include <netinet/in.h> 
#include <sys/types.h>
#include <sys/wait.h>

#include <app_router_ctrl.h>
#include <libsocket.h>
#include <libevent.h>

#include "../../ipcmsg/include/ipcmsg.h"

/*---------------------------------------------------------------------------------------------------------------------
 * DEFINES
 *--------------------------------------------------------------------------------------------------------------------*/

#define BUFFER_SIZE 3

/*---------------------------------------------------------------------------------------------------------------------
 * GLOBAL VARIABLES
 *--------------------------------------------------------------------------------------------------------------------*/

//static const char g_path_fms_binary[sizeof "app_fms/app_fms"] = "app_fms/app_fms";

/*---------------------------------------------------------------------------------------------------------------------
 * INTERNAL FUNCTIONS
 *--------------------------------------------------------------------------------------------------------------------*/

/* Callback function invoked upon reception of data. */
libevent_handler_status_t app_router_data_received(void *p_socket)
{
    fprintf(stdout, "Parent: Received data.\n");

    return LIBEVENT_HANDLER_STATUS_STOP;
}

/*---------------------------------------------------------------------------------------------------------------------
 * MAIN
 *--------------------------------------------------------------------------------------------------------------------*/

int main (int p_argc, char *p_argv[])
{
    libevent_t l_loop = {0};

    app_router_t l_app_context = {0};
    app_router_cfg_t l_app_context_cfg = 
    {
        .loop = &l_loop,
        .nr_processes = 1,
        .on_data_received = app_router_data_received
    };

    app_router_ctrl_init_system(&l_app_context, &l_app_context_cfg);
    app_router_ctrl_start_system(&l_app_context);

    return 0;
}
