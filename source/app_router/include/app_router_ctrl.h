/**
 * This header files defines all the public functions from app_router.
 *
 * @date 2018
 *
 * @page License
 *
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL X LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*---------------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------------*/
#ifndef APP_ROUTER_CTRL_H
#define APP_ROUTER_CTRL_H

#include "../../protobuf/protofiles/msg_ipc.pb-c.h"
#include "../../ipcmsg/include/ipcmsg.h"

#include <libsocket.h>
#include <libevent.h>

/** Forward declaration of app_router_t. */
typedef struct app_router_t app_router_t;

/** Forward declaration of app_router_cfg_t. */
typedef struct app_router_cfg_t app_router_cfg_t;

/** App_router's configuration structure. */
struct app_router_cfg_t
{
    /** Application eventloop. */
    libevent_t *loop;

    /** Number of child prcesses to spawn. */
    int32_t nr_processes;

    /** Callback function when data is received. */
    libevent_handler_status_t (*on_data_received)(void *p_this);
}; 

/** App_router's handle.*/
struct app_router_t
{
    /** Communication sockets. */
    socket_t socket;

    /** App_router's configuration. */
    app_router_cfg_t cfg;
}; 

/**
 * This function initializes the system and waits for a 
 * groundstation to connect.
 *
 * @param p_this The application's context.
 * @param p_cfg  The application's configuration.
 */
void app_router_ctrl_init_system(app_router_t *p_this, app_router_cfg_t *p_cfg);

/**
 * This function starts the system by spawning all processes.
 *
 * @param p_this The application's context.
 */
void app_router_ctrl_start_system(app_router_t *p_this);

/**
 * This function sends messages from the groundstation to the necessary process.
 */
void app_router_ctrl_send_msg_to_process();

/**
 * This function sends message from processes to the user/groundstation.
 */
void app_router_ctrl_send_msg_to_ground_station();

#endif
